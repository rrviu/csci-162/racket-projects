#lang Racket


(define (getLetter-c) (cond
                        [(not (char? y))
                         (set! y (read))
                         (getLetter-c) ]
                        [(not (char=? y #\c))
                         (set! y (read))
                           (getLetter-c) ] ))
                              

(define y (read))
(getLetter-c)

(printf "\n\n You typed ~a" y)
                                 